// Глобальные переменные
const startPeopleCount = 5;
let citiesAmount = 0;
let wholeWorld = {};

// Функция генерации цвета
function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let randomColor = '#';

    for (let i = 0; i < 6; i++) {
        randomColor += letters[Math.floor(Math.random() * 16)];
    }

    return randomColor;
}

// Функция генерации имени
function generateName() {
    let name = "";
    let possible = "01";

    for (let i = 0; i < 5; i++)
        name += possible.charAt(Math.floor(Math.random() * possible.length));

    return name;
}

// Создание массива человечков и сохранение их в глобальной переменной
createCity = (peopleCount, generalArrayElement) => {
    citiesAmount++;
    let peoples = {};

    for (let i = 1; i <= peopleCount; i++) {
        peoples[i] = {
            id: i,
            name: generateName(),
            color: getRandomColor()
        }
    }

    wholeWorld[citiesAmount] = peoples;
};

// Заселение человечков на страницу и сохранение их DOM элемента в переменной
// TODO: возможно стоит поменять название на более подходящее
displayPeoples = (peoplesForDisplay) => {
    let wrapperPeoples = document.getElementsByClassName('peoples-container')[0];
    wrapperPeoples.innerHTML = '';

    for (let city in peoplesForDisplay) {
        let cityElement = document.createElement('div');
        wrapperPeoples.appendChild(cityElement);

        let addNewCitizen = document.createElement('input');
        addNewCitizen.setAttribute('type', "button");
        addNewCitizen.setAttribute('class', "newCitizen");
        addNewCitizen.setAttribute('value', "Add");
        wrapperPeoples.appendChild(addNewCitizen);
        addNewCitizen.addEventListener('click', addSingleCitizen);

        for (let people in peoplesForDisplay[city]) {
            let peopleElement = document.createElement('i');

            peopleElement.className = 'fas fa-male';
            peopleElement.setAttribute('data-id', peoplesForDisplay[city][people].id);
            peopleElement.setAttribute('data-name', peoplesForDisplay[city][people].name);
            peopleElement.setAttribute('style', 'color: ' + peoplesForDisplay[city][people].color);

            cityElement.appendChild(peopleElement);
        }
    }
};

// Изменение цвета человекчка
changePeopleColor = (peopleDOM) => {

};

getDomPeopleById = (peopleId) => {
    return peoples[peopleId].domElement;
};

// Функция, которая срабатывает при клике на человечка
clickedByPeople = () => {

};

// Функция, которая срабатывает при загрузке структуры документа (когда доступны
// DOM элементы
domContentLoaded = () => {
    createCity(startPeopleCount);
    displayPeoples(wholeWorld);
    console.log(wholeWorld);
};

document.addEventListener('DOMContentLoaded', domContentLoaded);
document.getElementById('addNewCity').addEventListener('click', domContentLoaded);

addSingleCitizen = () => {

}