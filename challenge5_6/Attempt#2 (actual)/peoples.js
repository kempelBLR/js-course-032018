// Глобальные переменные
const startPeopleCount = 5;
let peoples = {};

// Функция генерации цвета
function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let randomColor = '#';

    for (let i = 0; i < 6; i++) {
        randomColor += letters[Math.floor(Math.random() * 16)];
    }

    return randomColor;
}

// Функция генерации имения
function generateName() {
    let name = "";
    let possible = "01";

    for (let i = 0; i < 5; i++)
        name += possible.charAt(Math.floor(Math.random() * possible.length));

    return name;
}

// Создание массива человечков и сохранение их в глобальной переменной
createPeoples = (peopleCount) => {
    switch (peopleCount) {

        case 5 :
            for (let i = 1; i <= peopleCount; i++) {
                peoples[i] = {
                    id: i,
                    name: generateName(),
                    color: getRandomColor()
                };
            };
            break;

        case 1 :
            let counter = 0;

            for (let key in peoples) {
                counter++;
            };

            if (counter < 8) {
                peoples[++counter] = {
                        id: counter,
                        name: generateName(),
                        color: getRandomColor()
                    }
            }
            else {
                alert('Хорош!')
            };
            break;
    }
};

//Удаление человека
removeOneMan = (incomingObject) => {
    let counter = 0;

    for (let key in incomingObject) {
        counter++;
    };

    if (counter > 2) {
        delete peoples[counter];
    }
    else {
        alert('Адам и Ева');
    }
}

// Заселение человечков на страницу и сохранение их DOM элемента в переменной
// TODO: возможно стоит поменять название на более подходящее
renderPeoples = (peoplesForDisplay) => {
    let wrapperPeoples = document.getElementsByClassName('peoples-container')[0];

    wrapperPeoples.innerHTML = '';

    let addNewCitizen = document.createElement('input');
        addNewCitizen.setAttribute('type', "button");
        addNewCitizen.setAttribute('class', "newCitizen");
        addNewCitizen.setAttribute('value', "Add");
        wrapperPeoples.appendChild(addNewCitizen);
        addNewCitizen.addEventListener('click', function() {
            createPeoples(1);        
            renderPeoples(peoples);
            console.log(peoples);
        });

    let removeCitizen = document.createElement('input');
        removeCitizen.setAttribute('type', "button");
        removeCitizen.setAttribute('class', "removeCitizen");
        removeCitizen.setAttribute('value', "Remove");
        wrapperPeoples.appendChild(removeCitizen);
        removeCitizen.addEventListener('click', function() {
            removeOneMan(peoples);        
            renderPeoples(peoples);
            console.log(peoples);
        });

    for (let separateMan in peoplesForDisplay) {
        let peopleElement = document.createElement('i');

        peopleElement.className = 'fas fa-male';
        peopleElement.setAttribute('data-id', peoplesForDisplay[separateMan].id);
        peopleElement.setAttribute('data-name', peoplesForDisplay[separateMan].name);
        peopleElement.setAttribute('style', 'color: ' + peoplesForDisplay[separateMan].color);

        wrapperPeoples.appendChild(peopleElement);

        peoples[separateMan].domElement = peopleElement;
    }
};

// Изменение цвета человекчка
changePeopleColor = (peopleDOM) => {
    peopleDOM.target.style.color = getRandomColor();
};

getDomPeopleById = (peopleId) => {
    return peoples[peopleId].domElement;
};

// Функция, которая срабатывает при клике на человечка
clickedByPeople = () => {

};

// Функция, которая срабатывает при загрузке структуры документа (когда доступны
// DOM элементы
domContentLoaded = () => {
    createPeoples(startPeopleCount);
    renderPeoples(peoples);
    console.log(peoples);
};

document.addEventListener('DOMContentLoaded', domContentLoaded);