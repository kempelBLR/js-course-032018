const initialInputValue = document.getElementById("resultValue");
const initialStepSize = document.getElementById("stepVolume");
let userValues = {};
let amountOfCounters = 1;
let newCountersArray = {};

document.addEventListener('DOMContentLoaded', function() {
    initialInputValue.value = 0;
    initialStepSize.value = 1;
    getCurrentInputValue();
});

getCurrentInputValue = () => {
    userValues["actualInputValue"] = +initialInputValue.value;
    userValues["actualStepSize"] = +initialStepSize.value;

    return userValues;
};

increaseCurrentValue = (currentValue) => {
    currentValue = userValues.actualInputValue + userValues.actualStepSize;
    currentValue = rangeValidation(currentValue);
    userValues.actualInputValue = currentValue;

    return userValues.actualInputValue;
};

decreaseCurrentValue = (currentValue) => {
    currentValue = userValues.actualInputValue - userValues.actualStepSize;
    currentValue = rangeValidation(currentValue);
    userValues.actualInputValue = currentValue;

    return userValues.actualInputValue;
};

keyboardDrivenChange = (e) => {
    let currentValue = getCurrentInputValue().actualInputValue;

    switch(e.keyCode) {
        case 37 :
            currentValue = decreaseCurrentValue(currentValue);
            break;
        case 39 :
            currentValue = increaseCurrentValue(currentValue);
            break;
    }

    return currentValue;
};

displayNewValue = (newValue) => {
    initialInputValue.value = newValue;
};

function rangeValidation(count) {
    if (count < 0) {
        count = 10;
    }
    else if (count > 10) {
        count = 0;
    }

    return count;
};


document.getElementById("stepVolume").addEventListener("input", getCurrentInputValue);
document.getElementById("resultValue").addEventListener("input", getCurrentInputValue);
document.getElementById("decreaseButton").addEventListener('click', function() {
    displayNewValue(decreaseCurrentValue());
});
document.getElementById("increaseButton").addEventListener('click', function() {
    displayNewValue(increaseCurrentValue());
});
window.addEventListener("keydown", function(e) {
    displayNewValue(keyboardDrivenChange(e));
});


addNewCounter = () => {
    amountOfCounters++;
    let newCountersWrapper = document.createElement('div.new-counters');
    newCountersWrapper.setAttribute('id', amountOfCounters);
    document.querySelector('body').appendChild(newCountersWrapper);

    let newDecreaseButton = document.createElement("input");
    newDecreaseButton.setAttribute('id', "decreaseButton" + amountOfCounters);
    newDecreaseButton.setAttribute('type', "button");
    newDecreaseButton.setAttribute('value', "Уменьшить -1");
    newDecreaseButton.setAttribute('style', "margin-right:5px;");
    newCountersWrapper.appendChild(newDecreaseButton);
    newDecreaseButton.addEventListener('click', newDecreaseCurrentValue);

    let newIncreaseButton = document.createElement("input");
    newIncreaseButton.setAttribute('id', "increaseButton" + amountOfCounters);
    newIncreaseButton.setAttribute('type', "button");
    newIncreaseButton.setAttribute('value', "Увеличить +1");
    newCountersWrapper.appendChild(newIncreaseButton);
    newIncreaseButton.addEventListener('click', newIncreaseCurrentValue);

    let newInputField = document.createElement("input");
    newInputField.setAttribute('id', "resultValue" + amountOfCounters);
    newInputField.setAttribute('type', "text");
    newInputField.setAttribute('style', "display:block;" + "margin-bottom:10px;" + "margin-top:5px;");
    newCountersWrapper.appendChild(newInputField);
    newInputField.value = 0;

    extendNewCountersArray(amountOfCounters, newInputField.value);
};

extendNewCountersArray = (newKey, newValue) => {
    newCountersArray[newKey] = +newValue;
};

document.getElementById("addCounter").addEventListener('click', addNewCounter);

newDecreaseCurrentValue = (e) => {
    const elemOwner = event.target.parentElement;
    const elemOwnerId = elemOwner.id;

    --newCountersArray[elemOwnerId];
    newCountersArray[elemOwnerId] = rangeValidation(newCountersArray[elemOwnerId]);

    displayAmendedValue(elemOwner, elemOwnerId);
    
};

newIncreaseCurrentValue = (e) => {
    const elemOwner = event.target.parentElement;
    const elemOwnerId = elemOwner.id;

    ++newCountersArray[elemOwnerId];
    newCountersArray[elemOwnerId] = rangeValidation(newCountersArray[elemOwnerId]);
    
    displayAmendedValue(elemOwner, elemOwnerId);
};

displayAmendedValue = (displayingElement, output) => {
    displayingElement.lastChild.value = newCountersArray[output];
};