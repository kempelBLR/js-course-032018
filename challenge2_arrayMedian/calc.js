const buttonResult = document.getElementById('result_button');
let medianDisplay = document.getElementById('result');

// Получение инпута пользователя
function getNumbers() {    
    let inputNumbers = document.getElementById("userInput").value;    
    console.log(inputNumbers + 'полученные числа');

    return inputNumbers;
} 
 
// Создание массива на основании полученного инпута
function createArray(inputString) {    
    let splittedString = inputString.split(' ');   
    console.log(splittedString + 'созданный массив');

    return splittedString;
} 

// Конвертация каждого элемента массива из строкового в численное значение
function convertArray(unconvertedArray) {    
    let convertedArray = unconvertedArray;

    for(let i=0; i<convertedArray.length;i++)     
        convertedArray[i] = +convertedArray[i];

    console.log(convertedArray + 'переконвертированный массив');

    return convertedArray;
} 

// Функция для сортировки массива
function compareNumbers(a, b) {    
    if (a > b) return 1;    
    if (a < b) return -1;
} 

// Сортировка массива
function sortArray(unsortedArray) {
    let sortedArray = unsortedArray.sort(compareNumbers);
    // convertedArray.sort( function(a,b) {return a - b;} );

    console.log(sortedArray + 'отсортированный массив');

    return sortedArray;
}

// Поиск медианы в массиве с четным количеством элементов
function evenCalc(evenArray) {
    let finArray = evenArray;
    let lowMiddle = Math.floor((finArray.length - 1) / 2);
    let highMiddle = Math.ceil((finArray.length - 1) / 2);
    let medianResult = (finArray[lowMiddle] + finArray[highMiddle]) / 2;

    console.log(medianResult + 'вычисленная медиана');

    return medianResult;
}

// Поиск медианы в массиве с нечетным количеством элементов
function oddCalc(unevenArray) {
    let finArray = unevenArray;
    let half = (finArray.length - 1) / 2;
    let medianResult = finArray[half];

    console.log(medianResult + 'вычисленная медиана');

    return medianResult;
}

// Нахождение медианы и вывод на экран
function calcMedian(preparedArray) {
    if (preparedArray.length % 2) {
        alert('Введено нечетное количество чисел:' + preparedArray.length);
        medianDisplay.innerHTML = oddCalc(preparedArray);
    }   else {
        alert('Введено четное количество чисел:' + preparedArray.length);
        medianDisplay.innerHTML = evenCalc(preparedArray);
    };

    return;
}




buttonResult.onclick = function() {
    calcMedian(sortArray(convertArray(createArray(getNumbers()))));
}