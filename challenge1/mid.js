const buttonResult = document.getElementById('result_button');
const midNumberDisplay = document.getElementById('result');

const number1 = document.getElementById('number1');
const number2 = document.getElementById('number2');
const number3 = document.getElementById('number3');
const errResult = "Equal numbers are not allowed.";

buttonResult.onclick = function() {

	let midNumber = +number3.value;

	if ((+number1.value > +number2.value) && (+number1.value < +number3.value)
		||
		(+number1.value > +number3.value) && (+number1.value < +number2.value)) {
        midNumber = +number1.value;
    }

    if ((+number2.value > +number1.value) && (+number2.value < +number3.value)
    	||
    	(+number2.value > +number3.value) && (+number2.value < +number1.value)) {
        midNumber = +number2.value;
    }

    if (+number1.value == +number2.value) {
    	alert("Number1 is equal to Number2. Mid number could not be calculated.")
    	midNumber = errResult;
    }

    if (+number1.value == +number3.value) {
    	alert("Number1 is equal to Number3. Mid number could not be calculated.")
    	midNumber = errResult;
    }

    if (+number2.value == +number3.value) {
    	alert("Number2 is equal to Number3. Mid number could not be calculated.")
    	midNumber = errResult;
    }

    midNumberDisplay.innerHTML = midNumber;
}