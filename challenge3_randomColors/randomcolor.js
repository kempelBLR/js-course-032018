const allRectanglesArray = document.querySelectorAll('.cell');

function getRandomColor() {
	let letters = '0123456789ABCDEF';
	let randomColor = '#';

for (let i = 0; i < 6; i++) {
	randomColor += letters[Math.floor(Math.random() * 16)];
	}

	return randomColor;
}

for (let i = 0; i < allRectanglesArray.length; i++) {
	allRectanglesArray[i].addEventListener('click', clickByRectangle);
};

function clickByRectangle() {
	this.style.backgroundColor = getRandomColor();
}